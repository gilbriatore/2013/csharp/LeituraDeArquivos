﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace LeituraDeArquivos
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Atividade: complete o código abaixo de forma que
             * todos movimentos que estão no arquivo de texto sejam
             * convertidos para objetos do tipo movimento/crédito ou
             * movimento/débito e colocados na lista que já foi criada.
             */ 

            //Cria um objeto stream.
            StreamReader stream = new StreamReader("extrato.txt");
            
            //Avança a primeira linha do arquivo.
            stream.ReadLine();

            //Lê a primeira linha do arquivo.
            string linha = stream.ReadLine();

            //Remove as aspas da linha.
            linha = linha.Replace("\"", "");

            //Quebra as informações da linha num vetor.
            string[] vetor = linha.Split(';');

            //Cria uma lista dinâmica que permite incluir os movimentos.
            List<Movimento> lista = new List<Movimento>();
            
            //Declara um objeto movimento.
            Movimento movimento;

            //Verifica se a linha que está no vetor é um débito.
            if (vetor[5].Trim() == "D")
            {
                //Cria um objeto movimento do tipo Debito.
                movimento = new Debito();
            }
            else
            {
                //Cria um objeto movimento do tipo Credito.
                movimento = new Credito();
            }

            //Define a conta no objeto movimento.
            movimento.DefinirConta(vetor[0]);

            //Separa o ano, mês e dia da string de data de movimento,
            //e converte a string em inteiros.
            int ano = Convert.ToInt16(vetor[1].Substring(0, 4));
            int mes = Convert.ToInt16(vetor[1].Substring(4, 2));
            int dia = Convert.ToInt16(vetor[1].Substring(6, 2));

            //Cria uma data de movimento com base nos valores.
            DateTime dataDeMovimento = new DateTime(ano, mes, dia);

            //Define a data de movimento no objeto movimento.
            movimento.DefinirDataDeMovimento(dataDeMovimento);

            //Define o número do documento no objeto movimento.
            movimento.DefinirNumeroDocumento(vetor[2]);

            //Define o o histórico no movimento.
            movimento.DefinirHistorico(vetor[3]);

            //Converte o valor para double.
            double valor = Convert.ToDouble(vetor[4].Replace('.', ','));

            //Define o valor no obejto movimento.
            movimento.DefinirValor(valor);
            
            //Adiciona o objeto movimento na lista dinâmica.
            lista.Add(movimento);
       
            //Parar
            //Console.WriteLine();

        }
    }
}
