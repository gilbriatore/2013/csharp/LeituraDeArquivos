﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LeituraDeArquivos
{
    class Movimento
    {
        private string conta;
        private DateTime dataDeMovimento;
        private string numeroDocumento;
        private string historico;
        private double valor;

        public void DefinirDataDeMovimento(DateTime data)
        {
            this.dataDeMovimento = data;
        }

        public void DefinirValor(double valor)
        {
            this.valor = valor;
        }

        public void DefinirHistorico(string historico)
        {
            this.historico = historico;
        }

        public void DefinirNumeroDocumento(string numero)
        {
            this.numeroDocumento = numero;
        }

        public void DefinirConta(string conta)
        {
            this.conta = conta;
        }
    }
}
